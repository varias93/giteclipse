package Negocio;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Grafo
{
	// Representamos el grafo por su matriz de adyacencia
	private int[][] adyacencia;
	private int vertices;
	  
	
	// La cantidad de vertices esta predeterminada desde el constructor
	public Grafo(int vertices)
	{
		this.vertices=vertices;		
		adyacencia = new int[vertices][vertices];
		for(int i=0; i<vertices;i++) {
			for(int j=0; j<vertices;j++) {
				adyacencia[i][j] = -1;
			}
		}
		
	}
	
	// Agregado de aristas
	public void agregarArista(int i, int j, int peso)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
		
		
		adyacencia[i][j] = peso ;
		adyacencia[j][i] = peso ;
	}
	
	// Eliminacion de aristas
	public void eliminarArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		adyacencia[i][j] = -1;
		adyacencia[j][i] = -1;
	}

	// Informa si existe la arista especificada
	public boolean existeArista(int i, int j)
	{
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);

		return adyacencia[i][j]>-1 ? true : false ;
	}

	// Cantidad de vertices
	public int tamano()
	{
		return adyacencia.length;
	}
	
	// Vecinos de un vertice
	public Set<Integer> vecinos(int i)
	{
		verificarVertice(i);
		
		Set<Integer> ret = new HashSet<Integer>();
		for(int j = 0; j < this.tamano(); ++j) if( i != j )
		{
			if( this.existeArista(i,j) )
				ret.add(j);
		}
		
		return ret;		
	}
	
	// Verifica que sea un vertice valido
	private void verificarVertice(int i)
	{
		if( i < 0 )
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		
		if( i >= adyacencia.length )
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}

	// Verifica que i y j sean distintos
	private void verificarDistintos(int i, int j)
	{
		if( i == j )
			throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")");
	}
	public void setAdyacencia(int[][] nueva) {
		this.adyacencia=nueva;			
	}	
	
	public void dividir() {		
		//verificar arista mas pesada y borrarla
		int verticeA=0;int verticeB=0;
		int max= Integer.MIN_VALUE;		
		for(int i = 0; i < vertices; i++) {
            for(int j = 0; j < vertices; j++) {
                if(max < adyacencia[i][j] && adyacencia[i][j]!=-1){
                    max = adyacencia[i][j];
                    verticeA= i;
                    verticeB= j;
                }
            }
		}
		eliminarArista(verticeA,verticeB);
		
	}
	public int[][] getAdyacencia() {
		return adyacencia;
	}

	@Override
	public String toString() {
		return "Grafo ["+ Arrays.deepToString(adyacencia) + "]";
	}

	public int getVertices() {
		return vertices;
	}

	public void setVertices(int vertices) {
		this.vertices = vertices;
	}	
}
